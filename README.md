# Swagger v3 Codegen Petstore Swagger2

Example of codegen Swagger v3 codegen first using Open Api Generator

## Implementation details

io.swagger.codegen.v3 swagger-codegen-maven-plugin
bug on generate spring-boot async responseWrapper CompletableFuture and reactive
no default interface
problem with java 14 + should use specific JAVA_OPTION --add-opens=java.base/java.util=ALL-UNNAMED

Open Api Generator more stable for spring boot application
